#define PLUGIN_VERSION "1.0"

public Plugin myinfo = {
	name = "Kraken - Player ELO",
	author = "Zipcore",
	description = "Kranken ELO system",
	version = PLUGIN_VERSION,
	url = "www.zipcore.net",
};

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <csgocolors>
#include <kraken>
#include <kraken-playerstats>
#include <kraken-elo>
#include <chat-processor>

Database g_dMain;

int g_iTotal;
int g_iELO[MAXPLAYERS + 1];
int g_iELORank[MAXPLAYERS + 1];

int g_iStatsID = -1;

Handle g_hTimer = null;

char g_sUnranked[256];
char g_sUnrankedFull[256];
char g_sUnrankedChat[256];

KeyValues g_kvRanks;
int g_iRankIndex[MAXPLAYERS + 1];
char g_sRankName[MAXPLAYERS + 1][256];
char g_sRankFull[MAXPLAYERS + 1][256];
char g_sRankChat[MAXPLAYERS + 1][256];

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iMaxError) 
{
	RegPluginLibrary("kraken-elo");
	
	CreateNative("Kraken_ELO_GetTotal", Native_GetTotal);
	CreateNative("Kraken_ELO_GetELO", Native_GetELO);
	CreateNative("Kraken_ELO_GetRank", Native_GetRank);
	
	Kraken_MarkNativesAsOptional();
	Kraken_ELO_MarkNativesAsOptional();
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	HookEvent("player_death", Event_PlayerDeath);
	
	LoadConfig();
	
	RegConsoleCmd("sm_elo", Cmd_ELO);
	RegConsoleCmd("sm_rank", Cmd_ELO);
	RegConsoleCmd("sm_ranks", Cmd_Ranks);
}

/* Database */

public void Kraken_OnDatabaseConnect(Database dDatabase, int iSID, int iGID, char[] sServerName)
{
	g_dMain = dDatabase;
	g_iStatsID = Kraken_PlayerStats_GetStatsID("kill");
	Kraken_PushQuery("CREATE TABLE IF NOT EXISTS `player_elo` (uid INT NOT NULL, elo INT NOT NULL, PRIMARY KEY (`uid`), INDEX uid_index USING BTREE (uid), INDEX elo_index USING BTREE (elo));");
	
	StartTimer();
	GetTotal();
}

/* Player */

public void OnClientDisconnect(int iClient) 
{
	g_iELO[iClient] = 0;
	g_iELORank[iClient] = 0;
	g_iRankIndex[iClient] = 0;
	g_sRankName[iClient] = g_sUnranked;
	g_sRankFull[iClient] = g_sUnrankedFull;
	g_sRankChat[iClient] = g_sUnrankedChat;
}

public void Kraken_OnClientLoaded(int iClient, int iUserID) 
{
	char sBuffer[512];
	Format(sBuffer, sizeof(sBuffer), "SELECT `elo` FROM `player_elo` WHERE uid = '%i';", iUserID);
	SQL_TQuery(g_dMain, Callback_LoadClientELO, sBuffer, GetClientUserId(iClient));
}

public void Callback_LoadClientELO(Handle hOwner, Handle hHndl, char[] sError, int iUserId) 
{
	if (hHndl == null) 
	{
		LogError("(LoadClientELO) - %s", sError);
		return;
	}
	
	int iClient;

	iClient = GetClientOfUserId(iUserId);
	if (!Kraken_IsValidClient(iClient))
		return; // Player disconnected
	
	int iUID = Kraken_GetUserID(iClient);
	if(iUID <= 0) 
		return; // This should not happen
	
	g_iELO[iClient] = 1000; // New player
	
	if(SQL_FetchRow(hHndl))
		g_iELO[iClient] = SQL_FetchInt(hHndl, 0);
	
	LoadELORank(iClient);
}

void LoadELORank(int iClient)
{
	char sBuffer[512];
	FormatEx(sBuffer, sizeof(sBuffer), "SELECT COUNT(*) FROM `player_elo` WHERE `elo` >= %i ORDER BY `elo` DESC", g_iELO[iClient]);
	SQL_TQuery(g_dMain, Callback_LoadClientELORank, sBuffer, GetClientUserId(iClient));
}

public void Callback_LoadClientELORank(Handle hOwner, Handle hHndl, char[] sError, int iUserId) 
{
	if (hHndl == null) 
	{
		LogError("(LoadClientELORank) - %s", sError);
		return;
	}
	
	int iClient;

	iClient = GetClientOfUserId(iUserId);
	if (!Kraken_IsValidClient(iClient))
		return; // Player disconnected
	
	g_iELORank[iClient] = 0;
	g_sRankName[iClient] = g_sUnranked;
	g_sRankFull[iClient] = g_sUnrankedFull;
	g_sRankChat[iClient] = g_sUnrankedChat;
	
	int iUID = Kraken_GetUserID(iClient);
	if(iUID <= 0) 
		return; // This should not happen
	
	if(SQL_FetchRow(hHndl))
		g_iELORank[iClient] = SQL_FetchInt(hHndl, 0);
	
	GetRank(iClient, false);
}

void LoadELORankAll()
{
	char sUserIDs[512];
	int iCount;
	
	for (int iClient = 1; iClient <= MaxClients; iClient++)
	{
		if(!Kraken_IsValidClient(iClient))
			continue;
		
		if(g_iELO[iClient] < 1)
			continue;
		
		if(Kraken_PlayerStats_GetStats(iClient, g_iStatsID) < 100)
			continue;
		
		int iUID = Kraken_GetUserID(iClient);
		if(iUID < 1)
			continue;
		
		iCount++;
		
		Format(sUserIDs, sizeof(sUserIDs), "%s%s%i", sUserIDs, iCount == 1 ? "" : ",", iUID);
	}
	
	if(iCount == 0)
		return;
	
	char sBuffer[2048];
	FormatEx(sBuffer, sizeof(sBuffer), "SELECT s.uid, s.place FROM (SELECT uid, @rownum:=@rownum + 1 AS place FROM player_elo JOIN (SELECT @rownum:=0) r ORDER BY elo DESC) s WHERE s.uid IN (%s);", sUserIDs);
	SQL_TQuery(g_dMain, Callback_LoadClientELORankAll, sBuffer);
}

public void Callback_LoadClientELORankAll(Handle hOwner, Handle hHndl, char[] sError, int data) 
{
	if (hHndl == null) 
	{
		LogError("(LoadClientELORankAll) - %s", sError);
		return;
	}
	
	while (SQL_FetchRow(hHndl))
	{
		int iClient = Kraken_GetClientOfUserID(SQL_FetchInt(hHndl, 0));
		
		if(Kraken_IsValidClient(iClient))
		{
			g_iELORank[iClient] = SQL_FetchInt(hHndl, 1);
			GetRank(iClient, true);
		}
	}
}

bool UpdateELO(int iClient)
{
	if(!Kraken_IsValidClient(iClient))
		return;
	
	int iUID = Kraken_GetUserID(iClient);
	if(iUID < 1)
		return;
	
	char sBuffer[512];
	FormatEx(sBuffer, sizeof(sBuffer), "INSERT INTO `player_elo` (uid, elo) VALUES (%i, %i) ON DUPLICATE KEY UPDATE elo = %i;", iUID, g_iELO[iClient], g_iELO[iClient]);
	SQL_TQuery(g_dMain, Callback_UpdateELO, sBuffer, GetClientUserId(iClient));	
}

public void Callback_UpdateELO(Handle hOwner, Handle hHndl, char[] sError, int iUserId) 
{
	if (hHndl == null) 
	{
		LogError("(UpdateELO) - %s", sError);
		return;
	}
	
	int iClient;

	iClient = GetClientOfUserId(iUserId);
	if (!Kraken_IsValidClient(iClient))
		return; // Player disconnected
	
	int iUID = Kraken_GetUserID(iClient);
	if(iUID <= 0) 
		return; // This should not happen
	
	LoadELORank(iClient);
}

/* Events */

public Action Event_PlayerDeath(Handle event, const char[] name, bool dontBroadcast)
{
	int iVictim = GetClientOfUserId(GetEventInt(event, "userid"));
	int iAttacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	int iAssister = GetClientOfUserId(GetEventInt(event, "assister"));
	ELO(iAttacker, iVictim, iAssister);
	
	return Plugin_Continue;
}

/* Calc. ELO */

void ELO(int iAttacker, int iVictim, int iAssister)
{
	if(!iAttacker || !IsClientInGame(iAttacker) || !iVictim || !IsClientInGame(iVictim))
		return; // Ignore invalid players
	
	if(IsFakeClient(iAttacker))
		g_iELO[iAttacker] = 500; // bots count as low players
	
	if(IsFakeClient(iVictim))
		g_iELO[iVictim] = 500; // bots count as low players
	
	if(g_iELO[iAttacker] < 1 || g_iELO[iVictim] < 1)
		return; // player not loaded yet
	
	// Attacker
	
	int iEloDiff = CalcElo(iAttacker, iVictim);
	if(iEloDiff <= 0)
		return; // Get ELO difference
	
	g_iELO[iAttacker] += iEloDiff; // Give attacker ELO diff as points
	
	// Victim
	
	float fCapELO = 2000.0;	// scale ELO the victim can loose based on his current ELO
							// 1000 = *0.5 2000 = *1-0 4000 = *2.0
	int iEloLoose = 1 + RoundToFloor(float(iEloDiff) * (float(g_iELO[iVictim]) / fCapELO))
	if(iEloLoose > 30)		// Max ELO a player can loose
		iEloLoose = 30;
	
	g_iELO[iVictim] -= iEloLoose; // Decrease victims ELO
	
	if(g_iELO[iVictim] < 1)
		g_iELO[iVictim] = 1; // Poor guy, ROFL
	
	UpdateELO(iAttacker);
	UpdateELO(iVictim);
		
	// Assister
	
	if(iAssister && IsClientInGame(iAssister) && !IsFakeClient(iAssister))
	{
		int iAssisterEloDiff = RoundToFloor((float(CalcElo(iAssister, iVictim))*0.6));
		
		if(iAssisterEloDiff < 1)
			iAssisterEloDiff = 1;
		
		g_iELO[iVictim] += iAssisterEloDiff;
		UpdateELO(iAssister);
	}
}

int CalcElo(int iAttacker, int iVictim)
{
	float fMaxDiff = 25.0; // Max ELO difference
	float fMinKills = 100.0; // Attacker min Kills 100 = 100%, 30 = 30% 
	float fMinKills2 = 10.0; // Victime min Kills 10 = 100%, 3 = 30%
	
	float fAttImpact = 1.0; // Get attacker's impact scale
	float fAttELO = float(g_iELO[iAttacker]);
	float fAttKills = float(Kraken_PlayerStats_GetStats(iAttacker, g_iStatsID));
	
	if(fAttKills < fMinKills) 
		fAttImpact = fAttKills / fMinKills;
	
	float fVicImpact = 1.0; // Get victims' impact scale
	float fVicELO = float(g_iELO[iVictim]);
	float fVicKills = float(Kraken_PlayerStats_GetStats(iVictim, g_iStatsID));
	
	if(fVicKills < fMinKills2)
		fVicImpact = fVicKills / fMinKills2;
	
	// Get ELO difference including attacker's & victim's implact scale
	float prob = 1/(Pow(10.0, (fVicELO-fAttELO)/400)+1);
	return RoundToFloor(fAttImpact*fVicImpact*fMaxDiff*(1-prob));
}

/* Total/Rank updater */

void StartTimer()
{
	if(g_hTimer != null)
		delete g_hTimer;
	
	g_hTimer = CreateTimer(30.0, Timer_UpdateTotal, _, TIMER_REPEAT);
}

public Action Timer_UpdateTotal(Handle timer)
{
	GetTotal();
	LoadELORankAll();
	
	return Plugin_Continue;
}

void GetTotal()
{
	SQL_TQuery(g_dMain, Callback_LoadTotal, "SELECT COUNT(*) FROM `player_elo` WHERE 1");
}

public void Callback_LoadTotal(Handle hOwner, Handle hHndl, char[] sError, int data) 
{
	if (hHndl == null) 
	{
		LogError("(LoadTotal) - %s", sError);
		return;
	}
	
	if(SQL_FetchRow(hHndl))
		g_iTotal = SQL_FetchInt(hHndl, 0);
}

/* Commands */

public Action Cmd_ELO(int iClient, int iArgs)
{
	if(!Kraken_IsValidClient(iClient))
		return Plugin_Handled;
	
	int iKills = Kraken_PlayerStats_GetStats(iClient, Kraken_PlayerStats_GetStatsID("kill"));
	
	if(iKills < 100)
	{
		CPrintToChat(iClient, "{lime}You need %i more kills to become ranked.", 100 - iKills);
		return Plugin_Handled;
	}
	
	if(g_iRankIndex[iClient] > 0)
	{
		CPrintToChatAll("{purple}[ELO] {darkred}%N{lime}'s {orange}Score: {darkred}%i {orange}Rank: {darkred}%i/%i {orange}Rankname: %s", iClient, g_iELO[iClient], g_iELORank[iClient], g_iTotal, g_sRankFull[iClient]);
		return Plugin_Handled;
	}
	
	CPrintToChat(iClient, "{lime}Rank loading or you are still unranked.");
	
	return Plugin_Handled;
}

/* Chat */

public Action CP_OnChatMessage(int& author, ArrayList recipients, char[] flagstring, char[] name, char[] message, bool& processcolors, bool& removecolors)
{
	if(strlen(g_sRankChat[author]))
		Format(name, MAXLENGTH_NAME, " {lime}[%s{lime}]%s{teamcolor}%s", g_sRankChat[author], strlen(g_sRankChat[author]) ? " " : "", name);
	return Plugin_Changed;
}

/* Config */

void LoadConfig()
{
	g_kvRanks = CreateKeyValues("Rankings");
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/kraken/elo-rankings.cfg");
	
	FileToKeyValues(g_kvRanks, sPath);
}

bool GetRank(int iClient, bool show)
{
	if(g_iELORank[iClient] < 1)
		return false;
	
	if(Kraken_PlayerStats_GetStats(iClient, g_iStatsID) < 100)
		return false;
	
	float fPos = float(g_iELORank[iClient]) / float(g_iTotal) * 100.0;
	
	if(g_iELORank[iClient] == 1)
		fPos = 0.0;
	
	int iIndex;
	g_kvRanks.GotoFirstSubKey(false);
	do 
	{
		iIndex++;
		
		if(g_kvRanks.GetFloat("default") == 1)
		{
			g_kvRanks.GetSectionName(g_sUnranked, sizeof(g_sUnranked));
			g_kvRanks.GetString("full", g_sUnrankedFull, sizeof(g_sUnrankedFull));
			g_kvRanks.GetString("chat", g_sUnrankedChat, sizeof(g_sUnrankedChat));
		}
		else if(g_kvRanks.GetFloat("pos") >= fPos)
		{
			char sName[256], sRankFull[256], sRankChat[256];
			
			g_kvRanks.GetSectionName(sName, sizeof(sName));
			g_kvRanks.GetString("full", sRankFull, sizeof(sRankFull));
			g_kvRanks.GetString("chat", sRankChat, sizeof(sRankChat));
			
			if(show && g_iRankIndex[iClient] != iIndex)
			{
				CPrintToChat(iClient, "{lightblue}************");
				
				if(g_iRankIndex[iClient] > iIndex)
					CPrintToChat(iClient, "{purple}[ELO] {darkblue}You {orange}up-ranked {lime}to: %s", sRankFull);
				else CPrintToChat(iClient, "{purple}[ELO] {darkblue}You {lightred2}de-ranked {lime}to: %s", sRankFull);
				
				CPrintToChat(iClient, "{lightblue}************");
				
				CPrintToChat(iClient, "{green}Use {darkred}!elo{green} in chat to show your rank to everyone & {darkred}!ranks{green} to get a list of all rank {orange}c{lightblue}o{darkblue}l{purple}o{darkred}r{orange}s{lime}!");
				
				CPrintToChat(iClient, "{lightblue}************");
			}
			
			g_iRankIndex[iClient] = iIndex;
			g_sRankName[iClient] = sName;
			g_sRankFull[iClient] = sRankFull;
			g_sRankChat[iClient] = sRankChat;
			g_kvRanks.Rewind();
			return true;
		}
		
	} while (g_kvRanks.GotoNextKey(false));
	g_kvRanks.Rewind();
	
	return false;
}

public Action Cmd_Ranks(int iClient, int iArgs)
{
	Menu_Ranks(iClient);
	return Plugin_Handled;
}

void Menu_Ranks(int iClient, int iPos = 0)
{
	Handle menu = CreateMenu(MenuHandler_Ranks);
	SetMenuTitle(menu, "Ranks");
	
	int iRank2 = 1;
	
	int iIndex;
	g_kvRanks.GotoFirstSubKey(false);
	do 
	{
		if(g_kvRanks.GetFloat("default") == 0)
		{
			iIndex++;
			
			char sName[256], sBuffer[256];
			g_kvRanks.GetSectionName(sName, sizeof(sName));
			
			float fPos = g_kvRanks.GetFloat("pos");
			
			int iRank = RoundToFloor(fPos * (float(g_iTotal)/100.0));
			
			char sRanks[32];
			
			if(fPos == 0.0)
			{
				sRanks = "Rank: #1";
				iRank = 1;
			}
			else if(iRank == iRank2 + 1)
				Format(sRanks, sizeof(sRanks), "Rank: #%i", iRank2);
			else Format(sRanks, sizeof(sRanks), "Ranks: #%i -> #%i", iRank2, iRank);
			
			Format(sBuffer, sizeof(sBuffer), "%s\n%s", sName, sRanks);
			
			char sInfo[11];
			IntToString(iIndex, sInfo, sizeof(sInfo));
				
			AddMenuItem(menu, sInfo, sBuffer);
			
			iRank2 = iRank+1;
		}
	} while (g_kvRanks.GotoNextKey(false));
	g_kvRanks.Rewind();
	
	if(iPos != 0)
		DisplayMenuAtItem(menu, iClient, iPos, MENU_TIME_FOREVER);
	DisplayMenu(menu, iClient, MENU_TIME_FOREVER);
}

public MenuHandler_Ranks(Handle menu, MenuAction action, int iClient, int info)
{
	if(action == MenuAction_Select)
	{
		int iIndex;
		char sInfo[64];
		GetMenuItem(menu, info, sInfo, sizeof(sInfo));
		int iIndex2 = StringToInt(sInfo);
		
		g_kvRanks.GotoFirstSubKey(false);
		do 
		{
			if(g_kvRanks.GetFloat("default") == 0)
			{
				iIndex++;
				
				if(iIndex2 == iIndex)
				{
					char sRankFull[256];
					g_kvRanks.GetString("full", sRankFull, sizeof(sRankFull));
					
					CPrintToChat(iClient, ">>> %s", sRankFull);
					
					g_kvRanks.Rewind();
					Menu_Ranks(iClient, GetMenuSelectionPosition());
					return;
				}
			}
			
		} while (g_kvRanks.GotoNextKey(false));
		g_kvRanks.Rewind();
	}
	else if(action == MenuAction_End)
		CloseHandle(menu);
}

/* Natives */

public int Native_GetELO(Handle hPlugin, int iParams) 
{
	return g_iELO[GetNativeCell(1)];
}

public int Native_GetRank(Handle hPlugin, int iParams) 
{
	return g_iELORank[GetNativeCell(1)];
}

public int Native_GetTotal(Handle hPlugin, int iParams) 
{
	return g_iTotal;
}