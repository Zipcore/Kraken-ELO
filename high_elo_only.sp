

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <csgocolors>
#include <kraken>
#include <kraken-playerstats>
#include <kraken-elo>
#include <smlib>
#include <chat-processor>

#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && !IsFakeClient(%1))

Handle g_hTimer[MAXPLAYERS + 1] = { null, ... };

public APLRes AskPluginLoad2(Handle hMyself, bool bLate, char[] sError, int iMaxError) 
{
	Kraken_MarkNativesAsOptional();
	Kraken_ELO_MarkNativesAsOptional();
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	LoopIngamePlayers(iClient)
		OnClientPostAdminCheck(iClient)
}

public void OnClientPostAdminCheck(int iClient)
{
	if(iClient < 1 || IsFakeClient(iClient) || IsClientSourceTV(iClient))
		return;
	
	if(g_hTimer[iClient] == null)
		g_hTimer[iClient] = CreateTimer(1.0, Timer_Check, iClient, TIMER_REPEAT);
}

public Action Timer_Check(Handle tiomer, any iClient)
{
	if(!IsClientInGame(iClient))
	{
		g_hTimer[iClient] = null;
		return Plugin_Stop;
	}
	
	int iELO = Kraken_ELO_GetELO(iClient);
	if(iELO <= 0)
		return Plugin_Continue;
	
	if(iELO < 1400 && !Client_HasAdminFlags(iClient, ADMFLAG_RESERVATION) && !Client_HasAdminFlags(iClient, ADMFLAG_BAN) && !Client_HasAdminFlags(iClient, ADMFLAG_ROOT))
		KickClient(iClient, "Your ELO score is not high enough, sorry!");
	
	g_hTimer[iClient] = null;
	
	return Plugin_Stop;
}