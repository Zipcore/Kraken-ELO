native int Kraken_ELO_GetTotal();
native int Kraken_ELO_GetELO(int iClient);
native int Kraken_ELO_GetRank(int iClient);

stock void Kraken_ELO_MarkNativesAsOptional()
{
	MarkNativeAsOptional("Kraken_ELO_GetTotal");
	MarkNativeAsOptional("Kraken_ELO_GetELO");
	MarkNativeAsOptional("Kraken_ELO_GetRank");
}